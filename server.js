var express = require('express');
var app = module.exports = express();
app.use(require('body-parser')());

/**
 *
 * @param {Function} onPush
 * @param {Number} port
 * @param {Array<String>} keys
 * @param {String} [messageMustContain]
 */
module.exports = function (onPush, port, keys, messageMustContain) {
    var server = app.listen(port, function () {

        app.post('*', function(req, res){
            if (keys.indexOf(req.query.key) !== -1) {
                var payload = req.body.payload;
                if (messageMustContain) {
                    var commits = payload.commits;
                    var index = commits.length;
                    while(index--) {
                        if (commits[index].message.indexOf(messageMustContain) !== -1) {
                            onPush(payload);
                            break;
                        }
                    }
                } else {
                    onPush(payload);
                }

                res.send(200);
            } else {
                res.send(401);
            }


        });
        console.info("Express server listening for POST requests on port %d", this.address().port);
    });
};
